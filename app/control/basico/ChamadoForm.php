<?php

use Adianti\Widget\Form\TText;

class ChamadoForm extends TPage
{
    protected $form;
    private $formFields = [];
    private static $database = 'atendimento';
    private static $activeRecord = 'Chamado';
    private static $primaryKey = 'id';
    private static $formName = 'form_ChamadoForm';

    /**
     * Form constructor
     * @param $param Request
     */
    public function __construct( $param )
    {
        parent::__construct();

        if(!empty($param['target_container']))
        {
            $this->adianti_target_container = $param['target_container'];
        }

        // creates the form
        $this->form = new BootstrapFormBuilder(self::$formName);
        // define the form title
        $this->form->setFormTitle("Cadastro de chamado");


        $id = new TEntry('id');
        $categoria_id = new TDBCombo('categoria_id', 'atendimento', 'Categoria', 'id', '{nome}','nome asc'  );
        $usuario = new TEntry('usuario');
        $data_abertura = new TDateTime('data_abertura');
        $data_fechamento = new TDateTime('data_fechamento');
        $titulo = new TEntry('titulo');
        $descricao = new TText('descricao');
        $email = new TEntry('email');

        $categoria_id->addValidation("Categoria id", new TRequiredValidator()); 
        $data_abertura->addValidation("Data de abertura do chamado", new TRequiredValidator()); 
        $data_fechamento->addValidation("Data de finalização do chamado", new TRequiredValidator()); 
       
        $email->addValidation("Email", new TEmailValidator());

        $id->setEditable(false);
        $categoria_id->enableSearch();
        $data_abertura->setMask('dd/mm/yyyy hh:ii');
        $data_abertura->setDatabaseMask('yyyy-mm-dd hh:ii');
        $data_fechamento->setMask('dd/mm/yyyy hh:ii');
        $data_fechamento->setDatabaseMask('yyyy-mm-dd hh:ii');

        $email->setMaxLength(255);
        $titulo->setMaxLength(255);
        $usuario->setMaxLength(255);

        $id->setSize(200);
        $email->setSize('100%');
        $titulo->setSize('100%');
        $usuario->setSize('100%');
        $descricao->setSize('100%');
        $data_abertura->setSize(150);
        $data_fechamento->setSize(150);
        $categoria_id->setSize('100%');

       
        
        $row2 = $this->form->addFields([new TLabel("Categoria:", '#ff0000', '14px', null, '100%'),$categoria_id]);
        $row2->layout = ['col-sm-12'];
        
        $row3 = $this->form->addFields([new TLabel("Usuário:", null, '14px', null, '100%'),$usuario]);
        $row3->layout = ['col-sm-12'];
        
        $row4 = $this->form->addFields([new TLabel("E-mail do usuário:", null, '14px', null, '100%'),$email]);
        $row4->layout = ['col-sm-12'];

        $row5 = $this->form->addFields([new TLabel("Data de abertura:", '#ff0000', '14px', null, '100%'),$data_abertura],[new TLabel("Data de fechamento:", '#ff0000', '14px', null, '100%'),$data_fechamento]);
        $row5->layout = ['col-sm-6','col-sm-6'];

        $row6 = $this->form->addFields([new TLabel("Título do chamado:", null, '14px', null, '100%'),$titulo]);
        $row6->layout = ['col-sm-12'];

        $row7 = $this->form->addFields([new TLabel("Descrição do chamado:", null, '14px', null, '100%'),$descricao]);
        $row7->layout = ['col-sm-12'];
        
        $row1 = $this->form->addFields([new TLabel("Código do chamado:", null, '14px', null, '100%'),$id]);
        $row1->layout = ['col-sm-12'];

        // create the form actions
        $btn_onsave = $this->form->addAction("Salvar", new TAction([$this, 'onSave']), 'fas:save #ffffff');
        $this->btn_onsave = $btn_onsave;
        $btn_onsave->addStyleClass('btn-primary'); 

        $btn_onclear = $this->form->addAction("Limpar formulário", new TAction([$this, 'onClear']), 'fas:eraser #dd5a43');
        $this->btn_onclear = $btn_onclear;

        $btn_onshow = $this->form->addAction("Voltar", new TAction(['ChamadoHeaderList', 'onShow']), 'fas:arrow-left #000000');
        $this->btn_onshow = $btn_onshow;

        parent::setTargetContainer('adianti_right_panel');

        $btnClose = new TButton('closeCurtain');
        $btnClose->class = 'btn btn-sm btn-default';
        $btnClose->style = 'margin-right:10px;';
        $btnClose->onClick = "Template.closeRightPanel();";
        $btnClose->setLabel("Fechar");
        $btnClose->setImage('fas:times');

        $this->form->addHeaderWidget($btnClose);

        parent::add($this->form);

    }

    public function onSave($param = null) 
    {
        try
        {
            TTransaction::open(self::$database); // open a transaction

            $messageAction = null;

            $this->form->validate(); // validate form data

            $object = new Chamado(); // create an empty object 

            $data = $this->form->getData(); // get form data as array
            $object->fromArray( (array) $data); // load the object with data

            $object->store(); // save the object 

            $loadPageParam = [];

            if(!empty($param['target_container']))
            {
                $loadPageParam['target_container'] = $param['target_container'];
            }

            // get the generated {PRIMARY_KEY}
            $data->id = $object->id; 

            $this->form->setData($data); // fill form data
            TTransaction::close(); // close the transaction

            TToast::show('success', "Registro salvo", 'topRight', 'far:check-circle');
            TApplication::loadPage('ChamadoHeaderList', 'onShow', $loadPageParam); 

                        TScript::create("Template.closeRightPanel();"); 
        }
        catch (Exception $e) // in case of exception
        {
            //</catchAutoCode> 

            new TMessage('error', $e->getMessage()); // shows the exception error message
            $this->form->setData( $this->form->getData() ); // keep form data
            TTransaction::rollback(); // undo all pending operations
        }
    }

    public function onEdit( $param )
    {
        try
        {
            if (isset($param['key']))
            {
                $key = $param['key'];  // get the parameter $key
                TTransaction::open(self::$database); // open a transaction

                $object = new Chamado($key); // instantiates the Active Record 

                $this->form->setData($object); // fill the form 

                TTransaction::close(); // close the transaction 
            }
            else
            {
                $this->form->clear();
            }
        }
        catch (Exception $e) // in case of exception
        {
            new TMessage('error', $e->getMessage()); // shows the exception error message
            TTransaction::rollback(); // undo all pending operations
        }
    }

    /**
     * Clear form data
     * @param $param Request
     */
    public function onClear( $param )
    {
        $this->form->clear(true);

    }

    public function onShow($param = null)
    {

    } 

}

