PRAGMA foreign_keys=OFF; 

CREATE TABLE categoria( 
      id  INTEGER    NOT NULL  , 
      nome varchar  (255)   , 
 PRIMARY KEY (id)) ; 

CREATE TABLE chamado( 
      id  INTEGER    NOT NULL  , 
      categoria_id int   NOT NULL  , 
      usuario varchar  (255)   , 
      data_abertura datetime   NOT NULL  , 
      titulo varchar  (255)   , 
      descricao text   , 
      email varchar  (255)   , 
 PRIMARY KEY (id),
FOREIGN KEY(categoria_id) REFERENCES categoria(id)) ; 

 
 
  
