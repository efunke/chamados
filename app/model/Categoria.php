<?php

class Categoria extends TRecord
{
    const TABLENAME  = 'categoria';
    const PRIMARYKEY = 'id';
    const IDPOLICY   =  'serial'; // {max, serial}

    

    /**
     * Constructor method
     */
    public function __construct($id = NULL, $callObjectLoad = TRUE)
    {
        parent::__construct($id, $callObjectLoad);
        parent::addAttribute('nome');
        parent::addAttribute('ativo');
            
    }

    /**
     * Method getChamados
     */
    public function getChamados()
    {
        $criteria = new TCriteria;
        $criteria->add(new TFilter('categoria_id', '=', $this->id));
        return Chamado::getObjects( $criteria );
    }

    public function set_chamado_categoria_to_string($chamado_categoria_to_string)
    {
        if(is_array($chamado_categoria_to_string))
        {
            $values = Categoria::where('id', 'in', $chamado_categoria_to_string)->getIndexedArray('nome', 'nome');
            $this->chamado_categoria_to_string = implode(', ', $values);
        }
        else
        {
            $this->chamado_categoria_to_string = $chamado_categoria_to_string;
        }

        $this->vdata['chamado_categoria_to_string'] = $this->chamado_categoria_to_string;
    }

    public function get_chamado_categoria_to_string()
    {
        if(!empty($this->chamado_categoria_to_string))
        {
            return $this->chamado_categoria_to_string;
        }
    
        $values = Chamado::where('categoria_id', '=', $this->id)->getIndexedArray('categoria_id','{categoria->nome}');
        return implode(', ', $values);
    }

    
}

